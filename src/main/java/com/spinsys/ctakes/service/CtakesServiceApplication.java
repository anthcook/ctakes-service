package com.spinsys.ctakes.service;

import com.spinsys.ctakes.Pipeline;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CtakesServiceApplication {

	public static void main( String[] args )
	{
		boolean runPipeline = false;
		
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--pipeline")) {
				runPipeline = true;
				break;
			}
		}
		
		if (runPipeline) {
			try {
				Pipeline.main( args );
			}
			catch (Exception exc) {
				exc.printStackTrace();
			}
		}
		else {
			SpringApplication.run( CtakesServiceApplication.class, args );
		}
	}
}
