package com.spinsys.ctakes.service;

import com.spinsys.ctakes.Constants;
import com.spinsys.ctakes.Pipeline;
import com.spinsys.ctakes.PipelineException;
import java.util.Properties;
import org.apache.uima.resource.ResourceInitializationException;
import org.json.JSONException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.xml.sax.SAXException;


/**
 * @author acook@spinsys.com
 */
@Controller
@RequestMapping("/ctakes")
public class CtakesServiceController
{
	@PostMapping("/pipeline")
	public ResponseEntity<String> pipeline(
			@RequestHeader(value = "Accept", defaultValue = Constants.NOT_PROVIDED) String responseType,
			@RequestHeader(value = "UMLS-Key", defaultValue = Constants.NOT_PROVIDED) String apiKey,
			@RequestBody String text )
	{
		ResponseEntity response;
		
		if (responseType.equals(Constants.NOT_PROVIDED) || responseType.equals("*/*")) {
			response = PipelineResponse.NO_TYPE.getResponse( true );
		}
		else if (!responseType.equals(MediaType.APPLICATION_XML_VALUE) && !responseType.equals(MediaType.APPLICATION_JSON_VALUE)) {
			response = PipelineResponse.BAD_TYPE.getResponse( true );
		}
		else if (apiKey.equals(Constants.NOT_PROVIDED)) {
			response = PipelineResponse.NO_KEY.getResponse( responseType, true );
		}
		else {
			try {
				Properties config = new Properties();
				config.setProperty( Constants.UMLS_API_KEY, apiKey );
				Pipeline parser = new Pipeline( config );
				parser.processText( text );
				if (responseType.equals(MediaType.APPLICATION_JSON_VALUE)) {
					response = PipelineResponse.SUCCESS.getResponse( parser.getJsonResults(), responseType, false );
				}
				else {
					response = PipelineResponse.SUCCESS.getResponse( parser.getXmiResults(), responseType, false );
				}
			}
			catch (PipelineException exc) {
				if (exc.getCause() instanceof SAXException || exc.getCause() instanceof JSONException) {
					response = PipelineResponse.BAD_ENTITY.getResponse( exc );
				}
				else if (exc.getCause() instanceof ResourceInitializationException) {
					response = PipelineResponse.BAD_KEY.getResponse( exc );
				}
				else {
					response = PipelineResponse.APP_ERROR.getResponse( exc );
				}
			}
		}
		
		return response;
	}

	
	protected enum PipelineResponse {
		SUCCESS( HttpStatus.OK, "" ),
		NO_TYPE( HttpStatus.BAD_REQUEST, "Result type not provided." ),
		NO_KEY( HttpStatus.UNAUTHORIZED, "UMLS API key not provided." ),
		BAD_KEY( HttpStatus.FORBIDDEN, "UMLS API key not validated." ),
		BAD_TYPE( HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Result type not supported." ),
		BAD_ENTITY( HttpStatus.UNPROCESSABLE_ENTITY, "Request body not understood." ),
		APP_ERROR( HttpStatus.INTERNAL_SERVER_ERROR, "Application error." );
		
		protected ResponseEntity<String> getResponse( boolean isError )
		{
			return getResponse( null, null, isError );
		}
		
		protected ResponseEntity<String> getResponse( String type, boolean isError )
		{
			return getResponse( null, type, isError );
		}
		
		protected ResponseEntity<String> getResponse( String data, String type, boolean isError )
		{
			StringBuilder responseBody = new StringBuilder( message );
			if (data != null) {
				if (message.length() > 0) responseBody.append( "\n\n" );
				responseBody.append( data );
			}
			if (type == null) type = MediaType.TEXT_PLAIN_VALUE;
			String body = makeBody( responseBody.toString(), type, isError );
			HttpHeaders headers = new HttpHeaders();
			headers.add( "Content-Type", type );
			headers.add( "Content-Length", String.valueOf(body.length()) );
			
			return new ResponseEntity<>( body, headers, status );
		}
		
		protected ResponseEntity<String> getResponse( Exception exc )
		{
			String responseBody = null;
			if (exc != null) {
				responseBody = exc.getMessage();
			}
			
			return getResponse( responseBody, null, true );
		}
		
		protected String makeBody( String content, String type )
		{
			return makeBody( content, type, false );
		}
		
		protected String makeBody( String content, String type, boolean isError )
		{
			String tag = (isError ? "error" : "result");
			StringBuilder out = new StringBuilder();
			if (type.equals(MediaType.APPLICATION_JSON_VALUE)) {
				out.append( "{" ).append( tag ).append(": \"" ).
						append( content ).append( "\"}" );
			}
			else if (type.equals(MediaType.APPLICATION_XML_VALUE)) {
				out.append( "<" ).append( tag ).append( ">" ).
						append( content ).append( "</" ).append( tag ).append( ">" );
			}
			else {
				out.append( content );
			}
			
			return out.toString();
		}
		
		PipelineResponse( final HttpStatus status, final String message )
		{
			this.status = status;
			this.message = message;
		}
		
		private final HttpStatus status;
		private final String message;
	}
}
