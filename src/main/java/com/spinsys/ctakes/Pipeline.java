package com.spinsys.ctakes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.ctakes.core.cc.pretty.plaintext.PrettyTextWriter;
import org.apache.ctakes.core.pipeline.PipelineBuilder;
import org.apache.ctakes.core.pipeline.PiperFileReader;
import org.apache.ctakes.typesystem.type.textspan.Sentence;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.json.JSONObject;
import org.json.XML;
import org.json.JSONException;
import org.xml.sax.SAXException;


/**
 * A pipeline for processing text through a cTAKES clinical pipeline.
 * 
 * @author acook@spinsys.com
 */
public class Pipeline
implements Constants
{
	public static void main( String[] args )
	throws Exception
	{
		File inputFile = null;
		String apiKey = null;
		String responseType = null;
		for (String param : args) {
			if (param.startsWith("--key=")) {
				apiKey = param.split( "\\=" )[1];
			}
			else if (param.startsWith("--file=")) {
				inputFile = new File( param.split( "\\=" )[1] );
			}
			else if (param.startsWith("--output=")) {
				responseType = param.split( "\\=" )[1].toLowerCase();
			}
		}
		
		if (apiKey == null) {
			System.err.println( "UMLS API key not specified. Use: --key=<api-key>..." );
		}
		else if (inputFile == null) {
			System.err.println( "Input text file not specified. Use: --file=<path/to/file>..." );
		}
		else if (!inputFile.exists()) {
			System.err.println( String.format("Input file not found: %s.", inputFile) );
		}
		else if (responseType != null && (!responseType.equals("xml") && !responseType.equals("json"))) {
			System.err.println( String.format("Unsupported output type specified. Use: --output=[json|xml]...", inputFile) );
		}
		else {
			StringBuilder textBuff = new StringBuilder();
			try (BufferedReader reader =
					new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)))) {
				String line;
				while ((line = reader.readLine()) != null) {
					textBuff.append( line ).append( LINE_SEP );
				}
			}
			Properties config = new Properties();
			config.setProperty( UMLS_API_KEY, apiKey );
			Pipeline pl = new Pipeline( config );
			pl.processText( textBuff.toString() );
			
			System.out.println( "\nProcessed text:" );
			System.out.println( textBuff );
			
			if( pl.hasResults ) {
				if (responseType == null) {
					System.out.println( "\nFormatted results -" );
					System.out.println( pl.getPrettyResults() );
				}
				if (responseType == null || responseType.equals("xml")) {
					System.out.println( "\nXMI results -" );
					System.out.println( pl.getXmiResults() );
				}
				if (responseType == null || responseType.equals("json")) {
					System.out.println( "\nJSON results -" );
					System.out.println( pl.getJsonResults() );
				}
			}
			else {
				System.out.println( "No results returned." );
			}
		}
	}
	
	
	/**
	 * Creates a new instance for processing text.
	 * 
	 * @param config  the configuration properties.
	 * @throws PipelineException  if a processing pipeline can't be created.
	 */
	public Pipeline( Properties config )
	throws PipelineException
	{
		init( config );
	}
	
	/**
	 * Returns true if this instance has processing results.
	 * 
	 * @return
	 */
	public boolean hasResults()
	{
		return hasResults;
	}

	/**
	 * Processes the given text using the configured analysis engine.
	 * 
	 * @param text  the text to process.
	 * @throws PipelineException  if an error occurs in processing.
	 */
	public void processText( String text )
	throws PipelineException
	{
		try {
			reset();
			jcas.setDocumentText( text );
			ae.process( jcas );
			hasResults = (jcas.size() > 0);
		}
		catch (AnalysisEngineProcessException exc) {
			throw new PipelineException( "AEPException happened...", exc );
		}
	}
	
	/**
	 * Returns the result of the last processing run as an XMI document. Returns
	 * null if no results are available.
	 * 
	 * @return the processing results in XMI format, or null.
	 * @throws PipelineException  if an error occurs in results formatting.
	 */
	public String getXmiResults()
	throws PipelineException
	{
		if (xmiOut == null && hasResults) {
			try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
				XmiCasSerializer.serialize( jcas.getCas(), out );
				xmiOut = out.toString();
			}
			catch (SAXException exc) {
				throw new PipelineException( "SAXException happened...", exc );
			}
			catch (IOException exc) {
				throw new PipelineException( "IOException happened...", exc );
			}
		}
		
		return xmiOut;
	}

	/**
	 * Returns the result of the last processing run as JSON text. Returns null
	 * if no results are available.
	 * 
	 * @return the processing results in JSON format, or null.
	 * @throws PipelineException  if an error occurs in results formatting.
	 */
	public String getJsonResults()
	throws PipelineException
	{
		if (jsonOut == null && hasResults) {
			try {
				JSONObject xmlJSONObj = XML.toJSONObject( getXmiResults() );
				jsonOut = xmlJSONObj.toString( 4 );
			}
			catch (JSONException exc) {
				throw new PipelineException( "JSONException happened...", exc );
			}
		}
		
		return jsonOut;
	}

	/**
	 * Returns the result of the last processing run as formatted text. Returns
	 * null if no results are available.
	 * 
	 * @return the processing results as formatted text, or null.
	 * @throws PipelineException  if an error occurs in results formatting.
	 */
	public String getPrettyResults()
	throws PipelineException
	{
		if (prettyOut == null && hasResults) {
			StringWriter sw = new StringWriter();
			try (BufferedWriter writer = new BufferedWriter( sw )) {
				Collection<Sentence> sentences = JCasUtil.select( jcas, Sentence.class );
				for (Sentence sentence : sentences) {
					PrettyTextWriter.writeSentence( jcas, sentence, writer );
				}
				prettyOut = sw.toString();
			}
			catch (IOException exc) {
				throw new PipelineException( "IOException happened...", exc );
			}
		}
		
		return prettyOut;
	}
	
	/**
	 * Resets the instance for a new processing run.
	 */
	public void reset()
	{
		if (resultsMap != null) {
			resultsMap.clear();
			resultsMap = null;
		}
		xmiOut = null;
		jsonOut = null;
		prettyOut = null;
		hasResults = false;
		jcas.reset();
	}

	/**
	 * Initialize the instance for processing text.
	 *
	 * @param config  configuration parameters.
	 * @throws PipelineException  if a processing pipeline can't be initialized.
	 */
	protected void init( Properties config )
	throws PipelineException
	{
		this.config = config;
		
		String piperPath = config.getProperty( PIPER_KEY );
		if (piperPath == null) piperPath = DEFAULT_PIPER;
		
		for (Map.Entry<Object,Object> entry : config.entrySet()) {
			if (entry.getKey().equals(PIPER_KEY)) continue;
			System.setProperty( String.valueOf(entry.getKey()), String.valueOf(entry.getValue()) );
		}
		
		try {
			PiperFileReader reader = new PiperFileReader( piperPath );
			PipelineBuilder builder = reader.getBuilder();
			AnalysisEngineDescription pipeline = builder.getAnalysisEngineDesc();
			ae = UIMAFramework.produceAnalysisEngine( pipeline );
			jcas = ae.newJCas();
		}
		catch (ResourceInitializationException exc) {
			throw new PipelineException( "RIPException happened...", exc );
		}
		catch (UIMAException exc) {
			throw new PipelineException( "UIMAException happened...", exc );
		}
		catch (IOException exc) {
			throw new PipelineException( "IOException happened...", exc );
		}
	}
	
	
	private boolean hasResults;
	private String xmiOut;
	private String jsonOut;
	private String prettyOut;
	private Map<String,FeatureStructure> resultsMap;
	private Properties config;
	private JCas jcas;
	private AnalysisEngine ae;
}
