package com.spinsys.ctakes;


/**
 * @author acook@spinsys.com
 */
public interface Constants
{
	public static final String DEFAULT_PROCESSOR =
			"desc/ctakes-clinical-pipeline/desc/analysis_engine/AggregatePlaintextFastUMLSProcessor.xml";
	public static final String DEFAULT_PIPER =
			"org/apache/ctakes/clinical/pipeline/DefaultFastPipeline.piper";

	public static final String UMLS_API_KEY = "ctakes.umls_apikey";
	public static final String PROCESSOR_KEY = "pipeline.processor";
	public static final String PIPER_KEY = "piper.path";
	
	public static final String NOT_PROVIDED = "NOT_PROVIDED";
	public static final String LINE_SEP = System.getProperty( "line.separator" );
}
