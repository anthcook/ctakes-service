package com.spinsys.ctakes;


/**
 * Thrown to indicate a problem with cTAKES pipeline handling.
 * 
 * @author acook@spinsys.com
 */
public class PipelineException
extends Exception
{
	public PipelineException( String message )
	{
		super( message );
	}
	
	public PipelineException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
